<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<script src="http://code.jquery.com/jquery.min.js"></script>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
</head>
<body>
	<div class="petshop_wrap" style="display:none;">
		<div class="petshop_popup_breeds petshop_popup" >
			<div class="_blankspace">
				<div class="petshop_titletxt">반려품종 선택</div>
			</div>
		</div>
		<div class="petshop_popup_breeds_tabswrap">
			<div class="petshop_popup_breeds_tabs">
				<a href="#" class="_active">전체</a>
				<a href="#" >ㄱ~ㅁ</a>
				<a href="#" >ㅂ~ㅊ</a>
				<a href="#" >ㅌ~ㅎ</a>
			</div>
		</div>
		<div class="petshop_popup_breeds_listwrap">
			<div class="petshop_popup_breeds_list_active">
				<a href="">여러가지 팻종류 a 태그가 있음</a> 
				<a href="">여러가지 팻종류 a 태그가 있음</a> 
				<a href="">여러가지 팻종류 a 태그가 있음</a> 
				<a href="">여러가지 팻종류 a 태그가 있음</a> 
				<a href="">여러가지 팻종류 a 태그가 있음</a> 
				<a href="">여러가지 팻종류 a 태그가 있음</a> 
			</div>
		</div>
		<div class="petshop_btnset">
			<a href="#" class="petshop_sbig_bgray_mb30" >취소</a>
			<a href="#" class="petshop_sbig_bgray_mb30" >등록</a>
		</div>
	</div><!-- // petshop_wrap -->

	<div class="petshop_register">
		<div class="petshop_register top" id="topTemplate_1">
			<a class="petshop_register_photo _empty _dog" href="#">
				<i class="petshop_icon_big_dog"></i>
				<i class="petshop_icon_big_dog"></i>
				<i class="petshop_icon_big_dog"></i>
				
				<!-- 이미지 삽입 시 (_empty 클래스 제거) -->
				<span class="_img">
					<img src="http://placeholde.it/400x400/000">
				</span>
			</a>
			<div class="petshop_custom_radioset">
				<label class="petshop_custom_radio _selected"><i class="petshop_icon_small"><input type="radio" name="petshop_radio_1" value="dog"><span>강아지</span></i></label>
				<label class="petshop_custom_radio _selected"><i class="petshop_icon_small"><input type="radio" name="petshop_radio_1" value="dog"><span>고양이</span></i></label>
				<label class="petshop_custom_radio _selected"><i class="petshop_icon_small"><input type="radio" name="petshop_radio_1" value="dog"><span>기타</span></i></label>
			</div>
		</div>
		
		<div class="petshop_register_basicinfo" id="basicTemplate_1">
			<p class="petshop_mletxt_mb10">기본정보</p>
			<div class="petshop_input_text">
				<div class="petshop_inputset" id="defaultTemplate_1">
					<div class="petshop_input_text">
						<input type="text" class="class1" placeholder="이름" name="petNm"/>
						<i class="petshop_icon_small _check"></i>
					</div>
					<div class="petshop_input_text">
						<input type="text" class="class1" placeholder="생년월일" name="bymdDt"/>
						<i class="petshop_icon_small _check"></i>
					</div>
					<div class="petshop_input_text _radioset">
						<label class="petshop_button_radio"><input type="radio" name="petshop_radio_2" value="mail"><span>남</span></label>
						<label class="petshop_button_radio"><input type="radio" name="petshop_radio_2" value="femail"><span>여</span></label>
						<i class="petshop_icon_small _check"></i>
					</div>
					<div class="petshop_input_text _radioset">
						<a href="#" class="petshop_sel_breed">품종을 선택해 주세요</a>
						<i class="petshop_icon_small _check"></i>
					</div>
				</div>
				<div class="petshop_register_checkform">
					<label class="petshop_checkbox">
						<input type="checkbox" class="class5" placeholder="대표설정" name="petshop_input_checkbox_1" onclick="checkboxReset(event);"/>
						<span class="_checkboxwrap">대표설정 체크</span>
					</label>
					<span>|</span>
					<a href="#" class="petshop_del_pet" onclick="javascript:petShopRemove();">반려동물 삭제</a>
				</div>
				
				<a href="#" class="petshop_sbig_bgray_mb30" onclick="javascript:petShopAdd();">반려동물 추가등록</a>
				
				<div class="petshop_register_agree _mb10">
					<p>개인정보 수집 및 이용</p>
				</div>
				
				<!-- 나중에 쓸 변수 (개인정보 수집 이용 허용 체크값) -->
				<input type="hidden" id="collectionPurpose" value="0"/>
				<input type="hidden" id="collectionList" value="0"/>
				<input type="hidden" id="collectionTerm" value="0"/>
				
			</div><!--// petshop_input_text -->
		</div><!--// petshop_register_basicinfo -->
	</div><!-- // petshop_register -->
</body>
<script type="text/javascript">
var templateNo = 1;

$(document).ready(function(){
});

// 대표설정 클릭 하면 타는 함수
function checkboxReset(e){
	//alert(event.target.name);
	var divId = e.target.parentNode.parentNode.parentNode.parentNode.id;
	var checked = e.target.checked;
	console.log("checked : " , checked);
	if(checked){
		$('[name^=petshop_input_checkbox_1]').each(function(){
			
			//console.log("$(this).parent().attr('id') : ", $(this).parent().parent().parent().parent().attr("id"));
			//console.log("divId : ", divId);
			//console.log("test : " + $(this).closest('[id^=basicTemplate]').attr("id"));
			
			if($(this).closest('[id^=basicTemplate]').attr("id") === divId) {
				return true;	// each 문에서 continue;
			}else{
				$(this).prop("checked", false);
			}
		});
	}
}

// 반려동물 추가등록을 눌렀을 때 호출되는 함수 
function petShopAdd(){
	
	// 일단 갖고옴
	var topTemplate = $('#topTemplate_1').clone();
	var basicTemplate = $('#basicTemplate_1').clone();
	//console.log(template1.html());
	//console.log(topTemplate + basicTemplate);
	
	var tempDiv = 
		'<div class="petshop_register top" id="topTemplate_' + (++templateNo) + '">' 
		+ topTemplate.html()
		+ '</div>'
		+'<div class="petshop_register_basicinfo" id="basicTemplate_' + (templateNo) + '">' 
		+ basicTemplate.html()
		+ '</div>';
	$('#topTemplate_1').parent().append(tempDiv);
	
	modifyHtml();
}

// 반력동물 삭제를 눌렀을 때 호출되는 함수 
function petShopRemove(){
	
	// 삭제 할때 템플릿이 하나 남으면 삭제하지 않기 
	if($('[id^=topTemplate]').length !== 1){
		$('[id^=topTemplate]').last().remove();
		$('[id^=basicTemplate]').last().remove();
		
		templateNo = $('[id^=topTemplate]').length;
		
		modifyHtml();
	}
}

function modifyHtml(){
	// 하나의 폼일때는 top image랑 개인정보 수집이 다 있어야됨
	var length = $('[id^=topTemplate]').length;
	if(length === 1){
		$('[id^=topTemplate]').each(function(){
			$(this).find('.petshop_register_photo').css("display" , "block");
		});
		$('[id^=basicTemplate]').each(function(){
			$(this).find('.petshop_sbig_bgray_mb30').css("display" , "block");
			$(this).find('.petshop_register_agree').css("display" , "block");
		});
	// 두개이상의 폼일 때 
	}else{
		// 첫번째 폼에서는 밑에 두개만 삭제
		$('[id^=basicTemplate]').each(function(index){
			//console.log("index : " + Number(index));
			//console.log("length - 1 : " + (Number(length) - 1));
			if(index !== (Number(length) - 1)) {
				$(this).find('.petshop_sbig_bgray_mb30').css("display" , "none");
				$(this).find('.petshop_register_agree').css("display" , "none");
			}else{
				$(this).find('.petshop_sbig_bgray_mb30').css("display" , "block");
				$(this).find('.petshop_register_agree').css("display" , "block");
			}
		});
		
		$('[id^=topTemplate]').each(function(index){
			if(index === 0) {
				$(this).find('.petshop_register_photo').css("display" , "block");
			}else{
				$(this).find('.petshop_register_photo').css("display" , "none");
			}
		});
	}
}

// div 수만큼 서버 호출
function beforeSubmit(){
	
	var outCheck = false;
	
	// 벨리데이션 
	$('[name^=petNm]').each(function (){
		console.log("name안의 this : " , $(this).val());
		if(!$(this).val()){
			alert("이름을 입력하세요.");
			$(this).focus();
			
			outCheck = true;
			return false;
		}
	});
	
	if( outCheck ) return; 
	
	// 벨리데이션 
	$('[name^=bymdDt]').each(function (){
		console.log("bymdDt안의 this : " , $(this).val());
		if(!$(this).val()){
			alert("생년월일을 입력하세요.");
			$(this).focus();
			
			outCheck = true;
			return false;
		}
	});
	
	if( outCheck ) return;
	
	// 벨리데이션이 끝났을 때 이걸 탐 ( 인서트 반복문 )
	$('[id^=defaultTemplate]').each(function(){
		var name = $(this).find('.class1').val();
		var bymdDt = $(this).find('.class2').val();
		
		console.log("name : ", name , ", bymdDt : " , bymdDt);
		// 니 ajax 호출
		
	});
}
</script>
</html>