package com.tmffjtl.kakao;

import static java.util.stream.Collectors.toList;

import java.nio.CharBuffer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;
import java.util.PrimitiveIterator.OfInt;
import java.util.function.Consumer;
import java.util.function.Predicate;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Stream;

import lombok.AllArgsConstructor;
import lombok.Data;
/**
 * 다트 게임은 총 3번의 기회로 구성된다.
	각 기회마다 얻을 수 있는 점수는 0점에서 10점까지이다.
	점수와 함께 Single(S), Double(D), Triple(T) 영역이 존재하고 각 영역 당첨 시 점수에서 1제곱, 2제곱, 3제곱 (점수^1 , 점수^2 , 점수^3 )으로 계산된다.
	옵션으로 스타상(*) , 아차상(#)이 존재하며 스타상(*) 당첨 시 해당 점수와 바로 전에 얻은 점수를 각 2배로 만든다. 아차상(#) 당첨 시 해당 점수는 마이너스된다.
	스타상(*)은 첫 번째 기회에서도 나올 수 있다. 이 경우 첫 번째 스타상(*)의 점수만 2배가 된다. (예제 4번 참고)
	스타상(*)의 효과는 다른 스타상(*)의 효과와 중첩될 수 있다. 이 경우 중첩된 스타상(*) 점수는 4배가 된다. (예제 4번 참고)
	스타상(*)의 효과는 아차상(#)의 효과와 중첩될 수 있다. 이 경우 중첩된 아차상(#)의 점수는 -2배가 된다. (예제 5번 참고)
	Single(S), Double(D), Triple(T)은 점수마다 하나씩 존재한다.
	스타상(*), 아차상(#)은 점수마다 둘 중 하나만 존재할 수 있으며, 존재하지 않을 수도 있다.
	0~10의 정수와 문자 S, D, T, *, #로 구성된 문자열이 입력될 시 총점수를 반환하는 함수를 작성하라.
	
	입력 형식
	“점수|보너스|[옵션]”으로 이루어진 문자열 3세트.
	예) 1S2D*3T
	
	점수는 0에서 10 사이의 정수이다.
	보너스는 S, D, T 중 하나이다.
	옵선은 *이나 # 중 하나이며, 없을 수도 있다.
	
	예제	dartResult	answer	설명
	1	1S2D*3T	37	1^1 * 2 + 2^2 * 2 + 3^3
	2	1D2S#10S	9	1^2 + 2^1 * (-1) + 10^1
	3	1D2S0T	3	1^2 + 2^1 + 0^3
	4	1S*2T*3S	23	1^1 * 2 * 2 + 2^3 * 2 + 3^1
	5	1D#2S*3S	5	1^2 * (-1) * 2 + 2^1 * 2 + 3^1
	6	1T2D3D#	-4	1^3 + 2^2 + 3^2 * (-1)
	7	1D2S3T*	59	1^2 + 2^1 * 2 + 3^3 * 2
 * */

public class Exam2 {
	public static void main(String[] args) {
		String[] args1 = {"1S2D*3T", "1D2S#10S" , "1D2S0T" 
								, "1S*2T*3S" , "1D#2S*3S" , "1T2D3D#" , "1D2S3T*"};
		String args2 = args1[(int) (Math.random() * 10) % 6];
		System.out.println("args : " + args2);
		System.out.println("result : " + calcPoint(makeVO(args2)));
	}

	public static List<VO> makeVO(String args1) {

		List<String> list = Arrays.asList(args1.split(""));
		List<VO> list2 = new ArrayList<VO>();

		// 1. string 배열로 숫자와 보너스 옵션을 구분
		Pattern p = Pattern.compile("^[0-9]$");

		VO vo = new VO();
		for(int i =list.size() -1; i >=0; i--) {
			if(p.matcher(list.get(i)).find()) {
				// 1일 경우에는 뒷자리 체크
				if("1".equals(list.get(i)) && "0".equals(list.get(i+1))) continue;
				// 0일경우에만 앞자리 체크
				if("0".equals(list.get(i))) {
					if(i != 0 && "1".equals(list.get(i-1))) {
						vo.setPoint(10);
					}else {
						vo.setPoint(0);
					}
				}else {
					vo.setPoint(Integer.valueOf(list.get(i)));
				}
				list2.add(vo);
				vo = new VO();
			}else {
				if("S".equals(list.get(i))) vo.setBonus(1);
				if("D".equals(list.get(i))) vo.setBonus(2);
				if("T".equals(list.get(i))) vo.setBonus(3);
				if("#".equals(list.get(i)) || "*".equals(list.get(i))) vo.setOption(list.get(i));
			}
		}
		return list2;
	}
	
	public static int calcPoint(List<VO> list) {
		
		int sum = 0;
		boolean isStar = false;
		
		for(VO vo : list) {
			int returnval = (int) Math.pow(vo.getPoint(), vo.getBonus());
			if(isStar) returnval = returnval * 2;
			if(vo.getOption() == null ) isStar = false;
			if("#".equals(vo.getOption())) returnval = returnval * (-1);
			if("*".equals(vo.getOption()))
			{
				returnval = returnval * 2;
				isStar = true;
			}
			sum = returnval + sum;
		}
		
		return sum;
	}
}

@Data
class VO {
  private int point;
  private int bonus;
  private String option;
}
