package com.tmffjtl.kakao;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;

public class Exam1 {
	public static void main(String[] args) {

		// 한변의 길이 n과 정수 배열 arr1, arr2 가 들어오면 비트연산을 해서 결과값을 내보낸다.
		// ex) n=5, arr1 = [9, 20, 28, 18, 11] , arr2 = [30, 1, 21, 17, 28], 출력 =
		// ["#####","# # #", "### #", "# ##", "#####"]
		// 정수배열의 길이는 n 이하이다.
		final int number = 5;
		List<Integer> args1 = Arrays.asList(9, 20, 28, 18, 11);
		List<Integer> args2 = Arrays.asList(30, 1, 21, 17, 28);
		
		// 기존방식
		for (int i = 0; i < number; i++) {
			int t1 = calcOr(args1.get(i), args2.get(i));
			int[] t2 = calcBinarynumber(t1, number);
			returnHash(t2);
		}
		
		// 함수형
		List<ArgsFusion> argsFusion = new ArrayList<>();
		final ArgsFusionCreator<ArgsFusion> fusionArgsCreator = ArgsFusion::new;
		for (int j = 0; j < number; j++) {
			argsFusion.add(fusionArgsCreator.create(args1.get(j), args2.get(j)));
		}
		
		argsFusion.stream()
				.map(i ->  calcOr(i.getNumber1(), i.getNumber2()))
				.forEach(i -> returnHash(calcBinarynumber(i, number)));
	}
	
	private static int calcOr(int a, int b) {
		return a | b;
	}

	private static int[] calcBinarynumber(int n, int number) {
		int[] b = new int[number];
		int i = number-1;
		while (n != 1) {
			b[i--] = n % 2;
			n = n / 2;
		}
		b[i] = n;
		return b;
	}
	
	private static void returnHash(int[] t2) {
		for (int i = 0; i < t2.length; i++) {
			System.out.print(t2[i] == 0 ? "  " : "#");
		}
		System.out.println();
	}
}

@AllArgsConstructor
@Data
class ArgsFusion {
  private int number1;
  private int number2;
}

@FunctionalInterface
interface ArgsFusionCreator<T extends ArgsFusion> {
  T create(int num1, int num2);
}