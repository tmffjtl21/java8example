package com.tmffjtl.test;

import static java.util.stream.Collectors.groupingBy;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.IntStream;

public class TestJava8 {
	public static void main(String[] args) {
		int number = 5;
		boolean t = IntStream.range(2, number)
						.noneMatch(i -> number % i == 0);
		System.out.println(t);
		
		
		Map<String, Integer> scores = new HashMap<>();
		scores.put("hi", 1);
		scores.put("hi222", 1);
		scores.put("hi2", 2);
		System.out.println(scores.toString());
		
		Map<Integer, List<String>> byScores = groupyByScores(scores);
		System.out.println(byScores.toString());
		
		Map<Integer, List<String>> byScores2 = groupyByScores(scores);
		System.out.println(byScores2.toString());
		
	}
	
	public static Map<Integer, List<String>> groupyByScoresByFunctional(Map<String, Integer> scores){
		
		return scores.keySet()
							.stream()
							.collect(groupingBy(scores::get));
	}
	
	public static Map<Integer, List<String>> groupyByScores(Map<String, Integer> scores){
		
		Map<Integer, List<String>> byScores = new HashMap<>();
		
		for (String name : scores.keySet()) {
			int score = scores.get(name);
			
			List<String> names = new ArrayList<>();
			if(byScores.containsKey(scores))
				names = byScores.get(scores);
			
			names.add(name);
			byScores.put(score, names);
		}
		
		return byScores;
	}
	
}
