package com.tmffjtl.exam;

public class OopAndFpAnotherExample {

	public static void main(String[] args) {
		/*final CalculatorService calculatorService = new CalculatorService(new Addition());
		final int additionResult = calculatorService.calculate(1, 1);
		System.out.println(additionResult);
		
		final int subtractionResult = calculatorService.calculate(1, 1);
		System.out.println(subtractionResult);
		
		final int multiplicationResult = calculatorService.calculate(1, 1);
		System.out.println(multiplicationResult);
		
		final int divisionResult = calculatorService.calculate(8, 4);
		System.out.println(divisionResult);*/
		
		Calcuration calcuration = (i1, i2) -> i1+ i2;
		
		// functional
		final FpCalculatorService fpCalculatorService = new FpCalculatorService();
		System.out.println("addition : " + fpCalculatorService.calculate(calcuration , 11, 4));
		System.out.println("subtraction : " + fpCalculatorService.calculate((i1, i2) -> i1- i2, 11, 4));
		System.out.println("multiplication : " + fpCalculatorService.calculate((i1, i2) -> i1* i2, 11, 4));
		System.out.println("division : " + fpCalculatorService.calculate((i1, i2) -> i1/ i2, 20, 4));
		System.out.println("custom calc : " + fpCalculatorService.calculate((i1, i2) -> (i1+ i2)*i2, 11, 4));
		
	}
}

class CalculatorService {
	private Calcuration calcuration;
	
	public CalculatorService(final Calcuration calculation) {
		this.calcuration = calculation;
	}
	
	public int calculate(int num1, int num2) {
		if( num1 > 10 && num2 < num1 ) {
			return calcuration.calcurate(num1, num2);
		}else {
			throw new IllegalArgumentException("Invalid input num1 : " + num1 + ", num2 : " + num2);
		}
	}
	
	public int compute(int num1, int num2) {
		if( num1 > 10 && num2 < num1 ) {
			return calcuration.calcurate(num1, num2);
		}else {
			throw new IllegalArgumentException("Invalid input num1 : " + num1 + ", num2 : " + num2);
		}
	}
}

@FunctionalInterface
interface Calcuration{
	int calcurate(int num1, int num2);
}

class Addition implements Calcuration{
	@Override
	public int calcurate(int num1, int num2) {
		return num1 + num2;
	}
}

class Subtraction implements Calcuration{
	@Override
	public int calcurate(int num1, int num2) {
		return num1 - num2;
	}
}

class Multiplication implements Calcuration{
	@Override
	public int calcurate(int num1, int num2) {
		return num1 * num2;
	}
}

class Division implements Calcuration{
	@Override
	public int calcurate(int num1, int num2) {
		return num1 / num2;
	}
}

// Functional Class
class FpCalculatorService {
	public int calculate(Calcuration calcuration, int num1, int num2) {
		if( num1 > 10 && num2 < num1 ) {
			return calcuration.calcurate(num1, num2);
		}else {
			throw new IllegalArgumentException("Invalid input num1 : " + num1 + ", num2 : " + num2);
		}
	}
}
