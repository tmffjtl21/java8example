package com.tmffjtl.exam;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.Predicate;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class FunctionalInterfaceExample3 {
	public static void main(String[] args) {
		final List<Product> products = Arrays.asList(
			new Product(1L, "A", new BigDecimal("10.00")),
			new Product(2L, "B", new BigDecimal("55.50")),
			new Product(3L, "C", new BigDecimal("17.45")),
			new Product(4L, "D", new BigDecimal("20.00")),
			new Product(5L, "E", new BigDecimal("119.99"))
		);
		
		final BigDecimal twenty = new BigDecimal("20");
		
		log.debug("products > 20 :" + filter(products, product -> product.getPrice().compareTo(twenty) > 0	));
		
		log.debug("products <= 20 :" + filter(products, product -> product.getPrice().compareTo(twenty) <= 0).toString());
		
//		filter(list, predicate);
	}
	
	private static <T> List<T> filter(List<T> list , Predicate<T> predicate ) {
		final List<T> result = new ArrayList<>();
		for(final T t : list) {
			if(predicate.test(t)) {
				result.add(t);
			}
		}
		return result;
	}
}

@AllArgsConstructor
@Data
class Product {
	private Long id;
	private String name;
	private BigDecimal price;
}

class DiscountedProduct extends Product{
	public DiscountedProduct(Long id, String name, BigDecimal price) {
		super(id, name, price);
	}
}