package com.tmffjtl.exam;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.function.Supplier;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class FunctionalInterfaceExample {
	
	// functional interface란 ? 인터페이스안에 abstract method가 하나만 존재하는 인터페이스 ( 람다를 사용하기 위한 인터페이스 ) 
	
	public static void main(String[] args) {
		
		// Function : 파라미터가 하나면 Optional이다 
		Function<String, Integer> toInt = value -> Integer.parseInt(value);
		final Integer number = toInt.apply("100");
		System.out.println(number);
		
		// Identity는 왜 쓰는가
		final Function<Integer, Integer> identity = Function.identity();
		System.out.println(identity.apply(999));
		
		// Consumer : Function대신에 리턴이 없는경우에 사용, Function은 리턴이 필요 
		final Consumer<String> print = value -> System.out.println(value);
		final Consumer<String> greeting = value -> System.out.println("Hello " + value);
		print.accept("Hello");
		greeting.accept("taejin");
		
		// Predicate : object 생성을 할 필요없이 항상 리턴이 boolean 타입의 Function  
		// 양수인지 아닌지 확인하는 Predicate
		Predicate<Integer> isPositive = i -> i > 0;
		System.out.println(isPositive.test(1));
		
		List<Integer> numbers = Arrays.asList(-5, -4, -3, -2, -1, 0, 1,2,3,4,5);
		System.out.println("positive integers : " + filter(numbers, isPositive));
		System.out.println("lessThan3 integers : " + filter(numbers, i -> i < 3));
		
		// Supplier : 입력값이 없고 실행만 되는 Function
		final Supplier<String> helloSupplier = () -> "Hello ";
		System.out.println(helloSupplier.get() + "world");
		
		long start = System.currentTimeMillis();
		printIfValidIndex(0, () -> getVeryExpensiveValue());
		printIfValidIndex(-1, () -> getVeryExpensiveValue());
		printIfValidIndex(-2, () -> getVeryExpensiveValue());
		
		System.out.println("It took " + (System.currentTimeMillis() - start) + "ms");
	}
	
	private static<T> List<T> filter(List<T> list, Predicate<T> filter){
		List<T> numbers = new ArrayList<>();
		for(T input : list) {
			if(filter.test(input)) {
				numbers.add(input);
			}
		}
		return numbers;
	}
	
//	private static void printIfValidIndex(int number, String value) {
	private static void printIfValidIndex(int number, Supplier<String> valueSupplier) {
		if(number >= 0) {
			System.out.println("The value is " + valueSupplier.get() + ".");
		}else {
			System.out.println("Invalid");
		}
	}
	
	private static String getVeryExpensiveValue() {
		try {
			TimeUnit.SECONDS.sleep(3);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		// Let's just say it has very expensive calculation here!
		return "Kevin";
	}
}
