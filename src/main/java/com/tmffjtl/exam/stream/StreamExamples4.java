package com.tmffjtl.exam.stream;

import static java.util.stream.Collectors.joining;
import static java.util.stream.Collectors.toList;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collector;
import java.util.stream.Stream;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class StreamExamples4 {
	@SuppressWarnings("unchecked")
	public static void main(String[] args) {
		final List<Product> products =
		        Arrays.asList(
		            new Product(1L, "A", new BigDecimal("100.50")),
		            new Product(2L, "B", new BigDecimal("23.00")),
		            new Product(3L, "C", new BigDecimal("31.45")),
		            new Product(4L, "D", new BigDecimal("80.20")),
		            new Product(5L, "E", new BigDecimal("7.50"))
		        );
		
		System.out.println("Products.price >= 30 : " + 
				products.stream()
					.filter(product -> product.getPrice().compareTo(new BigDecimal("30")) >= 0)
					.collect(toList())
				);
		
		/*System.out.println("Products.price >= 30 : (with joining) : " +  
		
				products.stream()
					.filter(product -> product.getPrice().compareTo(new BigDecimal("30")) >= 0)
					.map(product -> product.getName()).forEach(System.out::println)
//					.collect(joiningByChar((char)8))
							
			);*/
		
		products.stream()
			.filter(product -> product.getPrice().compareTo(new BigDecimal("30")) >= 0)
			.map(product -> product.getName()).map(i -> {
				return i;
			});
		
		products.stream()
			.map(i -> i.getPrice())
			.reduce(BigDecimal::add)				// Element를 받아서 하나씩 줄여나가는 함수 
//			.reduce(BigDecimal.ZERO, (product1, product2) -> product1.add(product2))
			.ifPresent(System.out::println);
		
		OrderedItem oi = new OrderedItem(56L, new Product(55L, "123", new BigDecimal("29")), 2);
		System.out.println("=====================");
		Stream.of(oi).map(i -> i).forEach(System.out::println);
	}

	private static Collector joiningByChar(char c) {
		String delimiter = String.valueOf(c);
		return joining(delimiter);
	}
}

@AllArgsConstructor
@Data
class Product {
  private Long id;
  private String name;
  private BigDecimal price;
}

@AllArgsConstructor
@Data
class OrderedItem {
  private Long id;
  private Product product;
  private int quantity;

  public BigDecimal getTotalPrice() {
    return product.getPrice().multiply(new BigDecimal(quantity));
  }
}

@AllArgsConstructor
@Data
class Order {
  private Long id;
  private List<OrderedItem> items;

  public BigDecimal totalPrice() {
    return items.stream()
        .map(item -> item.getTotalPrice())
        .reduce(BigDecimal.ZERO, (price1, price2) -> price1.add(price2));
  }
}