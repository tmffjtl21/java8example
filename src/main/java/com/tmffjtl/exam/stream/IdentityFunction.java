package com.tmffjtl.exam.stream;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.Function;

public class IdentityFunction {
	/**입력값과 출력값이 같은 함수*/
	
	public static void main(String[] args) {
		// ex 1)
		final List<Integer>numbers = Arrays.asList(1,2,3,4,5);
		System.out.println(map(numbers, i -> i*2));
		System.out.println(map(numbers, Function.identity()));
	}
	
	private static <T, R> List<R> map(final List<T> list, final Function<T, R> mapper) {
		final List<R> result = new ArrayList<>();
		for(final T t : list) {
			result.add(mapper.apply(t));
		}
		return result;
	}
	
	@SuppressWarnings("unchecked")
	private static <T, R> List<R> mapOld(final List<T> list, final Function<T, R> mapper) {
		
		if(mapper == null) return (List<R>) list;
		
		final List<R> result = new ArrayList<>();
		for(final T t : list) {
			result.add(mapper.apply(t));
		}
		return result;
	}
}
