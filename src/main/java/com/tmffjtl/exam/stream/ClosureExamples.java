package com.tmffjtl.exam.stream;

// Closure란 non-local variable 의 변수까지 참조하는 함수  
// Multi thread에서 어떤 쓰레드가 접근할지 모르기 때문에 Thread safe 하게 final로만 선언 가능하게 함. ex) js에서 const
public class ClosureExamples {
	private int number = 999;		// object member 클래스 멤버변수 
	
	public static void main(String[] args) {
		new ClosureExamples().test2();
	}
	
	private void test1() {

		final int number = 100;		// Effectively Final 
		
		testClosure("Anonymous Class",  new Runnable() {
			@Override
			public void run() {
				System.out.println(ClosureExamples.this.number);	// 그냥 this를 쓰면 new Runnable 인터페이스의 this 
			}
		});
		
		testClosure("Lambda", () -> System.out.println(this.number));		// lambda 에서는 여기서 this를 쓰면 ClosureExamples의 this에 접근
																											// lambda 자체는 scope 이 없다.  위의 anonymous 처럼 블록이 없다. 이게 차이점 
	}
	
	private void test3() {

		System.out.println("\"ClosureExamples calling toString()\": " + toString());
		System.out.println("\"ClosureExamples calling toString(int, String)\": " + toString(1, "Hello"));

		testClosure("Anonymous Class", new Runnable() {
			@Override
			public void run() {
//        System.out.println("toString(int, String): " + toString(1, "Test"));
				System.out.println("toString(int, String) causes compile-time error");
				System.out
						.println("ClosureExamples.this.toString(int, String): " + ClosureExamples.toString(1, "Test"));
			}
		});
		testClosure("Anonymous Class", new Runnable() {
			@Override
			public void run() {
//        System.out.println("toString(int): " + toString(1));
				System.out.println("toString(int) causes compile-time error");
				System.out.println("ClosureExamples.this.toString(int, String): " + ClosureExamples.this.toString(1));
			}
		});

		testClosure("Lambda Expression",
				() -> System.out.println("this.toString(int, String): " + this.toString(1, "Test")));
		testClosure("Lambda Expression", () -> System.out.println("toString(int, String): " + toString(1, "Test")));
		testClosure("Lambda Expression", () -> System.out.println("this.toString(int): " + this.toString(1)));
		testClosure("Lambda Expression", () -> System.out.println("toString(int): " + toString(1)));
	}
	
	private void test2() {

		testClosure("Anonymous Class", new Runnable() {
			@Override
			public void run() {
				System.out.println("this.toString(): " + this.toString());
			}
		});
		testClosure("Anonymous Class", new Runnable() {
			@Override
			public void run() {
				System.out.println("ClosureExamples.this.toString(): " + ClosureExamples.this.toString());
			}
		});
		testClosure("Lambda Expression", () -> System.out.println("this.toString(): " + this.toString()));
	}
	
	private static void testClosure(final String name, final Runnable runnable) {
		System.out.println("===================================");
		System.out.println("Start : " + name + ": ");
		runnable.run();
		System.out.println("===================================");
	}
	
	@Override
	public String toString() {
		return new StringBuilder("ClosureExamples{").append("number=").append(number).append('}').toString();
	}
	
	public String toString(int number) {
		return "#" + number;
	}
	
	public static <T> String toString(int number, T value) {
		return "[" + number + "] The value is " + String.valueOf(value) + ".";
	}
}
