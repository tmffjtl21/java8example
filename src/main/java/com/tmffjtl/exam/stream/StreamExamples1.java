package com.tmffjtl.exam.stream;

import static java.util.stream.Collectors.*;

import java.math.BigInteger;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import ch.qos.logback.core.net.SyslogOutputStream;

public class StreamExamples1 {
	public static void main(String[] args) {
		
		// 무한생성 
//		IntStream.rangeClosed(1, 10).forEach(System.out::print);
//		IntStream.iterate(1,  i -> i+1).forEach(System.out::println);
//		Stream.iterate(BigInteger.ONE, i -> i.add(BigInteger.ONE)).forEach(i -> System.out.println(i + " "));
		
//		Stream.of(1,2,3,4,5).forEach(System.out::print);
		Stream.of(1,2,3,4,5).collect(toList());
		
		Stream.of(1,2,3,4,5).collect(toList());
		
	}
}
