package com.tmffjtl.exam;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Java8exampleApplication {

	public static void main(String[] args) {
		SpringApplication.run(Java8exampleApplication.class, args);
	}
}
