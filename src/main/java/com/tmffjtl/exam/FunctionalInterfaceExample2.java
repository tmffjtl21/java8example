package com.tmffjtl.exam;

import java.math.BigDecimal;

public class FunctionalInterfaceExample2 {
	public static void main(String[] args) {
		println("Area is ", 12, 20, (message, length, width) -> message + (length * width));
	
		BigDecimalToCurrency bigDecimalToCurrency = bd -> "$" + bd.toString();
		System.out.println(bigDecimalToCurrency.toCurrency(new BigDecimal("120.00")));
		
		final InvalidFunctionalInterface anonymous = new InvalidFunctionalInterface() {
			@Override
			public <T> String mkString(T value) {
				return value.toString();
			}
		};
		System.out.println("anonymous class : " + anonymous.mkString(123));
		
//		final InvalidFunctionalInterface invalidFunctionalInterface = value -> value.toString();
		
	}
	
	private static <T1, T2, T3> void println(T1 t1, T2 t2, T3 t3, Function3<T1, T2, T3, String> function) {
		System.out.println(function.apply(t1, t2, t3));
	}
}

@FunctionalInterface
interface Function3<T1,T2,T3,R> {
	R apply(T1 t1, T2 t2, T3 t3);
}

@FunctionalInterface
interface BigDecimalToCurrency {
	String toCurrency(BigDecimal value);
}

// 아래 인터페이스는 lambda 사용 불가능 
@FunctionalInterface
interface InvalidFunctionalInterface {
	<T> String mkString(T value);
}